.. currentmodule:: sardana.pool.pooltriggergate

:mod:`~sardana.pool.pooltriggergate`
====================================

.. automodule:: sardana.pool.pooltriggergate

.. rubric:: Classes

.. hlist::
    :columns: 3

    * :class:`PoolTriggerGate`

PoolTriggerGate
---------------

.. inheritance-diagram:: PoolTriggerGate
    :parts: 1
    
.. autoclass:: PoolTriggerGate
    :show-inheritance:
    :members:
    :undoc-members:
